﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VC.OVH
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Task<managerService> soapiTask=Task<managerService>.Run(() => new managerService());

			Console.WriteLine("OVH Login?");
			string login=Console.ReadLine();
			Console.WriteLine("OVH Password?");
			string password=Console.ReadLine();

			soapiTask.Wait();
			managerService soapi = soapiTask.Result;
			string session=soapi.login(login,password,"fr",false);
			if(string.IsNullOrEmpty(session))
			{ Console.WriteLine("Unable to login"); }

			try
			{
				Console.WriteLine("OK session "+session);

				//Enter keys here
				telephonyFunctionKeyStruct[] keys={
					new telephonyFunctionKeyStruct { function = "SUP0",keyNum = 01,relatedNumber = "0033123456789" },
					new telephonyFunctionKeyStruct { function = "SUP0",keyNum = 02,relatedNumber = "0033198765432" }
					//Other keys goes here
				};

				telephonyLineListReturn lines=soapi.telephonyLineList(session, null);
				foreach (telephonyLineStruct line in lines.link)
				{
					Console.WriteLine("Settings keys for line "+line.number);
					SetKeys(soapi, session, line.number, keys, line);
				}
			}
			finally
			{ soapi.logout(session); }
		}

		public static void SetKeys(managerService soapi,string session,string number,telephonyFunctionKeyStruct[] keys,telephonyLineStruct line=null)
		{
			//Sort the current keys in an array
			telephonyFunctionKeyStruct[] currentKeys = null;
			try
			{ currentKeys=soapi.telephonyFunctionKeyList(session,number,"fr"); }
			catch
			{ }
			{
				if (currentKeys == null || currentKeys.Length <= 0)
				{ return; }

				telephonyFunctionKeyStruct[] currentKeysTmp=new telephonyFunctionKeyStruct[currentKeys.Max(_key => _key.keyNum)+1];
				foreach(telephonyFunctionKeyStruct currentKey in currentKeys)
				{ currentKeysTmp[currentKey.keyNum] = currentKey; }
				currentKeys = currentKeysTmp;
			}

			if (line == null)
			{ line = soapi.telephonyLineDetails(session, number, "fr"); }

			foreach (telephonyFunctionKeyStruct key in keys)
			{
				if (key.keyNum < currentKeys.Length)
				{
					telephonyFunctionKeyStruct currentKey = currentKeys[key.keyNum];

					if (currentKey != null)	//If null => unasignable
					{
						if(string.IsNullOrEmpty(key.function))
						{ soapi.telephonyFunctionKeyDel(session,number,"fr",key.keyNum); }
						else
						{
							telephonyLineStruct targetLine = null;
							try
							{ targetLine=soapi.telephonyLineDetails(session,key.relatedNumber,"fr"); }
							catch
							{ }

							bool sameAccount = line != null && targetLine!=null && line.billingAccount == targetLine.billingAccount;

							string function = key.function;
							if(function == "SUP0" && !sameAccount)
							{ function = "DIAL"; }

							if(currentKey != null && !string.IsNullOrEmpty(currentKey.function))
							{
								if(function!=currentKey.function || currentKey.relatedNumber!=key.relatedNumber)
								{ soapi.telephonyFunctionKeyModify(session,number,"fr",key.keyNum,function,key.relatedNumber); }
							}
							else
							{ soapi.telephonyFunctionKeyAdd(session,number,"fr",key.keyNum,function,key.relatedNumber); }
						}
					}
				}
			}
		}
	}
}